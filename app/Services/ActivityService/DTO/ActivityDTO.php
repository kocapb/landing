<?php

namespace App\Services\ActivityService\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class ActivityDTO
 * @package App\Services\ActivityService\DTO
 */
class ActivityDTO extends DataTransferObject
{
    public string $url;
    public string $date;
}
