<?php


namespace App\Services\ActivityService;

use App\Services\ActivityService\Contract\TransportContract;
use App\Services\ActivityService\DTO\ActivityDTO;

/**
 * Class Client
 * @package App\Services\ActivityService
 */
class Client implements TransportContract
{
    private string $url;
    private string $method;

    public function __construct(string $url, string $method)
    {
        $this->url = $url;
        $this->method = $method;
    }

    public function send(ActivityDTO $dto): bool
    {
        $data = $this->dtoToJson($dto);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json','Content-Length: ' . strlen($data)]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->method);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        $response  = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function dtoToJson(ActivityDTO $dto): string
    {
        return json_encode($dto->toArray(), JSON_THROW_ON_ERROR);
    }
}
