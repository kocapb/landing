<?php

namespace App\Services\ActivityService\Contract;

use App\Services\ActivityService\DTO\ActivityDTO;

/**
 * Interface ActivityContract
 * @package App\Services\ActivityService\Contract
 */
interface TransportContract
{
    public function send(ActivityDTO $dto): bool;
}
