<?php

namespace App\Services\ActivityService;

use App\Services\ActivityService\Contract\TransportContract;
use App\Services\ActivityService\DTO\ActivityDTO;

/**
 * Class Service
 * @package App\Services\ActivityService
 */
class Service
{
    private TransportContract $transport;

    public function __construct(TransportContract $transport)
    {
        $this->transport = $transport;
    }

    public function send(ActivityDTO $dto): bool
    {
        return $this->transport->send($dto);
    }
}
