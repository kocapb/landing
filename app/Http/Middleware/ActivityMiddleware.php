<?php

namespace App\Http\Middleware;

use App\Jobs\ActivitySender;
use App\Services\ActivityService\DTO\ActivityDTO;
use App\Services\ActivityService\Service;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;

/**
 * Class ActivityMiddleware
 * @package App\Http\Middleware
 */
class ActivityMiddleware
{
    protected Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Bus::dispatch(new ActivitySender(new ActivityDTO([
            'url' => $request->getUri(),
            'date' => Carbon::now()->toDateTimeString(),
        ])));

        return $next($request);
    }
}
