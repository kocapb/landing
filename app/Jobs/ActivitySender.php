<?php

namespace App\Jobs;

use App\Services\ActivityService\DTO\ActivityDTO;
use App\Services\ActivityService\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ActivitySender implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected ActivityDTO $dto;

    public function __construct(ActivityDTO $dto)
    {
        $this->dto = $dto;
    }

    /**
     * Execute the job.
     *
     * @param Service $service
     *
     * @return void
     */
    public function handle(Service $service): void
    {
        $service->send($this->dto);
    }
}
