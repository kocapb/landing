<?php

namespace App\Providers;

use App\Services\ActivityService\Client;
use App\Services\ActivityService\Service;
use Illuminate\Support\ServiceProvider;

/**
 * Class ActivityServiceProvider
 * @package App\Providers
 */
class ActivityServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(Service::class, function ($app) {
            $options = config('services.activity');

            return new Service(
                $app->make(Client::class, $options),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
